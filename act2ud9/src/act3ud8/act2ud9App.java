package act3ud8;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import javax.sound.midi.Soundbank;

import dto.Entregable;
import dto.Serie;
import dto.Videojuego;


/**
 * @author Elisabet Sabat�
 *
 */
public class act2ud9App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		Serie[] series = new Serie[5];
		Videojuego[] videojuegos = new Videojuego[5];
		
		rellenarValores(series, videojuegos);
		
		series[0].entregar();
		series[2].entregar();
		series[4].entregar();
		videojuegos[1].entregar();
		videojuegos[2].entregar();
		videojuegos[3].entregar();
		
		ArrayList<Serie> seriesEntregadas = seriesEntregadas(series);
		ArrayList<Videojuego> videojuegosEntregados = videojuegosEntregados(videojuegos);
		
		int numSeries = mostrarSeriesEntregadas(seriesEntregadas);
		System.out.println("Total de series entregadas: " + numSeries);
		
		int juegosEntregados = mostrarvideojuegosEntregados(videojuegosEntregados);
		System.out.println("Total de videojocs entregats: " + juegosEntregados);

		System.out.println("--------------------------------------------------------"
				+ "\n--------------------------------------------------------");
		
		Videojuego videojuegoMasHoras = buscarVideojuego(videojuegos);
		System.out.println("\nVideojuego m�s horas: \n" + videojuegoMasHoras +"\n");
		
		Serie serieMasTemporadas = buscarSerie(series);
		System.out.println("Serie m�s temporadas: \n" + serieMasTemporadas +"\n\n");
	
	} 
	
	public static Serie buscarSerie(Serie[] series) {
		int contador = 1;
		Serie serieMasTemporadas = series[0];
		while(contador<series.length) {
			int resposta = series[contador].compareTo(series[contador-1]);
			if(resposta==1)
				serieMasTemporadas = series[contador];
			contador++;
		}
		return serieMasTemporadas;
	}
	
	
	public static Videojuego buscarVideojuego(Videojuego[] videojuegos) {
		int contador = 1;
		Videojuego videojuegoMasHoras = videojuegos[0];
		while(contador<videojuegos.length) {
			int resposta = videojuegos[contador].compareTo(videojuegos[contador-1]);
			if(resposta==1)
				videojuegoMasHoras = videojuegos[contador];
			contador++;
		}
		return videojuegoMasHoras;
	}
	

	
	public static int mostrarSeriesEntregadas(ArrayList<Serie> seriesEntregadas) {
		Serie[] seriesEntregadasArray = new Serie[seriesEntregadas.size()];
		seriesEntregadasArray = seriesEntregadas.toArray(seriesEntregadasArray);
		int numSerie = 0;
		System.out.println("-----------  SERIES ENTREGADES  ------------- ");
		for(Serie s : seriesEntregadasArray) {
			System.out.println("SERIE " + (numSerie+1) + ":");
			numSerie++;
			System.out.println(s);
		} 
		return numSerie;
	}
	
	public static int mostrarvideojuegosEntregados(ArrayList<Videojuego> juegosEntregados) {
		Videojuego[] juegosEntregadosArray = new Videojuego[juegosEntregados.size()];
		juegosEntregadosArray = juegosEntregados.toArray(juegosEntregadosArray);
		int numVideojoc = 0;
		System.out.println("\n\n-----------  VIDEOJUEGOS ENTREGADOS  ------------- ");
		for(Videojuego v : juegosEntregadosArray) {
			System.out.println("VIDEOJUEGO " + (numVideojoc+1) + ":");
			numVideojoc++;
			System.out.println(v);
		}
		return numVideojoc;	  
	}
	
	public static ArrayList<Serie> seriesEntregadas(Serie[] series) {
		
		ArrayList<Serie> seriesEntregadas = new ArrayList<Serie>();
		int contador = 0;
		while(contador<series.length) {
			if(series[contador].isEntregado())
				seriesEntregadas.add(series[contador]);
			contador++;
		}
		return seriesEntregadas;	
	}
	
	public static ArrayList<Videojuego> videojuegosEntregados(Videojuego[] videojuego) {
		
		ArrayList<Videojuego> videojuegosEntregados = new ArrayList<Videojuego>();
		int contador = 0;
		while(contador<videojuego.length) {
			if(videojuego[contador].isEntregado())
				videojuegosEntregados.add(videojuego[contador]);
			contador++;
		}
		return videojuegosEntregados;
	}
	
	public static void rellenarValores(Serie[] series, Videojuego[] videojuegos) {
		series[0] = new Serie("friends","Marta Kauffman");
		series[1] = new Serie("Malcolm in the middle","Frankie Muniz");
		series[2] = new Serie("Los Simpsons","Matt Groening");
		series[3] = new Serie("Padre de familia","comedia","Seth MacFarlane",18);
		series[4] = new Serie("American dad","comedia","Seth MacFarlane", 17);
		
		videojuegos[0] = new Videojuego("Last of us 2",30);
		videojuegos[1] = new Videojuego("Doom Eternal",25);
		videojuegos[2] = new Videojuego("Ghost of Tsushima",50);
		videojuegos[3] = new Videojuego("Halo infinite","disparos","343 Industries", 45);
		videojuegos[4] = new Videojuego("Resident evil 3","Horror","copmon", 6);
	}
	

}
