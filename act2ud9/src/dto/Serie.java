/**
 * 
 */
package dto;

/**
 * @author Elisabet Sabat�
 *
 */
public class Serie implements Entregable {
	
	
		private String titulo = "", genero ="", creador ="";
		private int numTemporadas = 3;
		private boolean entregado = false;
	
		
	public Serie() {
		
	}
	
	public Serie(String titulo, String creador) {
		this.titulo = titulo;
		this.creador = creador;
	}

	public Serie(String titulo, String genero, String creador, int numTemporadas) {
		this.titulo = titulo;
		this.genero = genero;
		this.creador = creador;
		this.numTemporadas = numTemporadas;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public int getNumTemporadas() {
		return numTemporadas;
	}

	public void setNumTemporadas(int numTemporadas) {
		this.numTemporadas = numTemporadas;
	}
	
	public void entregar() {
		entregado = true;
	}
	
	public void devolver() {
		entregado = false;
	}
	
	public boolean isEntregado() {
		return entregado;
	}
	
	public int compareTo(Object a) {
		Serie ref=(Serie)a;
        if (numTemporadas>ref.getNumTemporadas())
            return 1;
        else
        	return 0;
	}
	

	@Override
	public String toString() {
		return "\n-T�tulo: " + titulo +"\n-G�nero: " + genero +"\n-Creador: " + creador
				+ "\n-N�mero temporades: " + numTemporadas + "\n-Entregado: " + entregado +"\n\n";
	}

}
