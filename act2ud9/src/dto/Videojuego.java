package dto;

public class Videojuego implements Entregable{

	private String titulo = "",genero = "", companyia = "";
	private int horasEstimadas = 10;
	boolean entregado = false;
	
	public Videojuego() {
		
	}

	public Videojuego(String titulo, int horasEstimadas) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
	}

	public Videojuego(String titulo, String genero, String companyia, int horasEstimadas) {
		this.titulo = titulo;
		this.genero = genero;
		this.companyia = companyia;
		this.horasEstimadas = horasEstimadas;
		
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCompanyia() {
		return companyia;
	}

	public void setCompanyia(String companyia) {
		this.companyia = companyia;
	}

	public int getHorasEstimadas() {
		return horasEstimadas;
	}

	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}
	

	public void entregar() {
		entregado = true;
	}
	
	public void devolver() {
		entregado = false;
	}
	
	public boolean isEntregado() {
		return entregado;
	}
	
	public int compareTo(Object a) {
		Videojuego ref=(Videojuego)a;
        if (horasEstimadas>ref.getHorasEstimadas())
            return 1;
        else
        	return 0;
	}
	
	@Override
	public String toString() {
		return "\n-T�tulo: " + titulo +"\n-G�nero: " + genero +"\n-Compa��a: " + companyia
				+ "\n-Horas estimadas: " + horasEstimadas + "\n-Entregado: " + entregado +"\n\n";
	}


}
