package act1ud8;
import java.util.Random;
import java.util.Scanner;

import javax.sound.midi.Soundbank;

import dto.Electrodomestico;
import dto.Lavadora;
import dto.Television;
/**
 * @author Elisabet Sabat�
 *
 */
public class act1ud8App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		int precioBase = precioBase(teclat);
		int peso = peso(teclat);
		System.out.println("Introdueix el color: ");
		String color = teclat.next();
		char consumEnergetic = consumEnergetic(teclat);
		int carga = carga(teclat);
		int resolucion = resolucion(teclat);
		boolean sintetizadorTDT = true;
	
		Electrodomestico[] electrodomesticos = new Electrodomestico[10];
		electrodomesticos[0] = new Electrodomestico();
		electrodomesticos[1] = new Lavadora();
		electrodomesticos[2] = new Television();
		electrodomesticos[3] = new Electrodomestico(precioBase, peso);
		electrodomesticos[4] = new Lavadora(precioBase, peso);
		electrodomesticos[5] = new Television(precioBase, peso);
		electrodomesticos[6] = new Electrodomestico(precioBase, peso, color, consumEnergetic);
		electrodomesticos[7] = new Lavadora(precioBase, peso, color, consumEnergetic, carga);
		electrodomesticos[8] = new Television(precioBase, peso, color, consumEnergetic, resolucion, sintetizadorTDT);
		electrodomesticos[9] = new Television(precioBase, peso, color, consumEnergetic, resolucion, false);
	
		int contador = 0;
		System.out.println("Vost� t� " + electrodomesticos.length +" electrodom�stics en total");
		
		double totalElectro = 0, totalLavadora = 0, totalTelevision = 0, totalFinal = 0;
		System.out.println("TIPUS ELECTRODOM�STICO		PRECIO");
		while(contador<electrodomesticos.length) {
			
			if(electrodomesticos[contador] instanceof Lavadora) {
				System.out.println("Lavadora			"+electrodomesticos[contador].getPrecioFinal());
				totalLavadora = totalLavadora + electrodomesticos[contador].getPrecioFinal();
			} else if(electrodomesticos[contador] instanceof Television) {
				System.out.println("Televisi�n			"+electrodomesticos[contador].getPrecioFinal());
				totalTelevision = totalTelevision + electrodomesticos[contador].getPrecioFinal();
			} else {
				System.out.println("Altres				"+electrodomesticos[contador].getPrecioFinal());
				totalElectro = totalElectro + electrodomesticos[contador].getPrecioFinal();
			}
			contador++;
		}
		
		totalFinal = totalElectro + totalLavadora + totalTelevision;
	
		System.out.println("SUMA DEL TOTAL: (lavadores)"+totalLavadora + "	(televisi�n)"+totalTelevision +"	(Altres)"+totalElectro+" = " + totalFinal);
	}
	
	
	public static char consumEnergetic(Scanner teclat) {
		char consum = 'F';
		boolean consumEnergeticValid = false;
		while(!consumEnergeticValid) {
		System.out.println("Dinos el vostre consum energ�tic:");
		String consumUsuari = teclat.next();
		if(consumUsuari.length()==1) {
				consumEnergeticValid = true;
				consumUsuari = consumUsuari.toUpperCase();
				consum = consumUsuari.charAt(0);
		}
		else
			System.out.println("Sisplau, heu d'introduir un valor d'un car�cter, no us exediu.");
		}
		return consum;
	}
	
	public static int carga(Scanner teclat) {
		int carga = 0;
		boolean cargaValida = false;
		while(!cargaValida) {
		System.out.println("Dinos la carga:");
		carga = teclat.nextInt();
		if(carga>0)
			cargaValida = true;
		else
			System.out.println("Sisplau, heu d'introduir un valor positiu.");
		}
		return carga;
	}
	
	public static int precioBase(Scanner teclat) {
		int precioBase = 0;
		boolean precioBaseValid = false;
		while(!precioBaseValid) {
		System.out.println("Dinos el precio base:");
		precioBase = teclat.nextInt();
		if(precioBase>0)
			precioBaseValid = true;
		else
			System.out.println("Sisplau, heu d'introduir un valor positiu.");
		}
		return precioBase;
	}
	
	public static int peso(Scanner teclat) {
		int peso = 0;
		boolean pesoValido = false;
		while(!pesoValido) {
		System.out.println("Dinos el peso:");
		peso = teclat.nextInt();
		if(peso>0)
			pesoValido = true;
		else
			System.out.println("Sisplau, heu d'introduir un valor positiu.");
		}
		return peso;
	}
	
	public static int resolucion(Scanner teclat) {
		int resolucion = 0;
		boolean resoValido = false;
		while(!resoValido) {
		System.out.println("Dinos la resoluci�n:");
		resolucion = teclat.nextInt();
		if(resolucion>0)
			resoValido = true;
		else
			System.out.println("Sisplau, heu d'introduir un valor positiu.");
		}
		return resolucion;
	}

}
