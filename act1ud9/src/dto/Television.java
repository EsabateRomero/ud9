package dto;

public class Television extends Electrodomestico{

	private int resolucion = 20;
	private boolean sintetizadorTDT = false;
	
	public Television() {
		
	}
	public Television(int precio, int peso) {
		super(precio, peso);
		
	}
	public Television(int precioBase, int peso, String color, char consumEnergetic, int resolucion, boolean sintetizadorTDT) {
		super(precioBase, peso, color, consumEnergetic);
		this.resolucion = resolucion;
		this.sintetizadorTDT = sintetizadorTDT;
		precioFinal = precioFinal();
	}
	public int getResolucion() {
		return resolucion;
	}

	public boolean isSintetizadorTDT() {
		return sintetizadorTDT;
	}

	public double precioFinal() {
		if(resolucion>40)
			precioFinal = precioFinal +0.3*precioFinal;
		if(sintetizadorTDT)
			precioFinal = precioFinal + 50;
	
		return precioFinal;
	}
	
	

}
