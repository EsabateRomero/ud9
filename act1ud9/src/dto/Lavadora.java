package dto;

public class Lavadora extends Electrodomestico {

	private int carga = 5;
	
	public Lavadora() {
		if(precioFinal())
			this.precioFinal = precioFinal + 50;
	}

	public Lavadora(int precioBase, int peso, String color, char consumEnergetic, int carga) {
		super(precioBase, peso, color, consumEnergetic);
		if(precioFinal())
			this.precioFinal = precioFinal + 50;
		this.carga = carga;
	}

	public Lavadora(int precio, int peso) {
		super(precio, peso);
		if(precioFinal())
			this.precioFinal = precioFinal + 50;
	}

	public int getCarga() {
		return carga;
	}
	
	public boolean precioFinal() {
		if(carga>30)
			return true;
		else
			return false;
	}
	
	@Override
	public String toString() {
		return "-----\nPrecio base: " + precioBase + "\nPeso: " +  peso + "\nConsum Energetic: " + consumEnergetic + "\nColor del electrodoméstico: " +color +"\nCarga: "+carga+"\nPreu final: " + precioFinal;
	}
		
	

}
