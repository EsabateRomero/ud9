package dto;


public class Electrodomestico {
	
	final int A = 100;
	final int B = 80;
	final int C = 60;
	final int D = 50;
	final int E = 30;
	final int F = 10;
	
	final int TAM1 = 10;
	final int TAM2 = 50;
	final int TAM3 = 80;
	final int TAM4 = 100;
	
		
	protected int precioBase = 100, peso = 5; 
			
	protected double precioFinal;
	protected String color = "blanc";
	protected char consumEnergetic = 'F';
	
	public Electrodomestico() {
		this.precioFinal = precioFinal();
	}
	
	
	public Electrodomestico(int precio, int peso) {
		this.precioBase = precio;
		this.peso = peso;
		this.precioFinal = precioFinal();		
	}
	
	public Electrodomestico(int precioBase, int peso, String color, char consumEnergetic) {
		if(comprobarConsumoEnergetico(consumEnergetic))
			this.consumEnergetic = consumEnergetic;
		if(comprobarColor(color))
			this.color = color;
		this.precioBase = precioBase;
		this.peso = peso;	
		this.precioFinal = precioFinal();
	}
	
	private boolean comprobarColor(String color) {
		boolean colorValid = false;
		if(color.equalsIgnoreCase("blanc")||color.equalsIgnoreCase("negre")||color.equalsIgnoreCase("vermell")||color.equalsIgnoreCase("blau")||color.equalsIgnoreCase("gris"))
			colorValid = true;
	
		return colorValid;
	}
	
	private boolean comprobarConsumoEnergetico(char Letra) {
		boolean consumoEnergeticoValido = false;
		if((int)Letra>=65&&(int)Letra<=70)
			consumoEnergeticoValido = true;
			
		return consumoEnergeticoValido;
	}
	
	private int precioFinal() {
		int precioFinal = precioBase;
		
		if(consumEnergetic=='A')
			precioFinal = precioFinal + A;
		if(consumEnergetic=='B')
			precioFinal = precioFinal + B;
		if(consumEnergetic=='C')
			precioFinal = precioFinal + C;
		if(consumEnergetic=='D')
			precioFinal = precioFinal + D;
		if(consumEnergetic=='E')
			precioFinal = precioFinal + E;
		if(consumEnergetic=='F')
			precioFinal = precioFinal + F;
		
		if(peso>0&&peso<20)
			precioFinal = precioFinal + TAM1;
		if(peso>=20&&peso<50)
			precioFinal = precioFinal + TAM2;
		if(peso>=50&&peso<80)
			precioFinal = precioFinal + TAM3;
		if(peso>=80)
			precioFinal = precioFinal + TAM4;
		
		return precioFinal;
	}
	

	public double getPrecioFinal() {
		return precioFinal;
	}


	public void setPrecioFinal(double precioFinal) {
		this.precioFinal = precioFinal;
	}


	@Override
	public String toString() {
		return "-----\nPrecio base: " + precioBase + "\nPeso: " +  peso + "\nConsum Energetic: " + consumEnergetic + "\nColor del electrodoméstico: " +color +"\nPreu final: " + precioFinal;
	}
		
	
	
}
