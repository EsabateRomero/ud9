package act6ud9;

import java.util.Random;

import odt.Cine;
import odt.Espectador;
import odt.Pelicula;

public class act6ud9App {

	public static void main(String[] args) {
		
		Pelicula pelicula = new Pelicula();
		Cine cinema = new Cine(pelicula, 10, 3,4);
		crearPelicula(pelicula);
		System.out.println(pelicula);
		Espectador[] espectador = new Espectador[100];
		int cont = 0;
		while(cont<espectador.length) {
			espectador[cont] = crearEspectador();
			cont++;
		}
	
		System.out.println(cinema);
	}
	
	public static Espectador crearEspectador() {
		Random random = new Random();
		final String[] NOMBRES = {"Maria", "Joana", "Marina", "Pau", "Joan", "Jaume", "Antoni", "Rafael", "Mireia"};
		Espectador espect = new Espectador();
		
		espect.setNombre(NOMBRES[random.nextInt(NOMBRES.length-1)]);
		espect.setEdad(random.nextInt(50)+10);
		espect.setDinero(random.nextInt(100));

		return espect;
		
	}
	
	public static void crearPelicula(Pelicula pelicula) {
		Random random = new Random();
		
		final String[] TITULOS = {"La casa de la sangre","Animals i animalets","Les princeses disney", "sexe i amor"};
		final String[] DIRECTORES = {"Joan Kosoai","Mariona Perill","Sonia Mortadela", "Jaume Rocafort"};
		final int[] DURACION = {120, 130, 110, 140};
		final int[] EDADMIN = {18,3,3,16};
		int numPelicula = random.nextInt(4);
		pelicula.setTitulo(TITULOS[numPelicula]);
		pelicula.setDirector(DIRECTORES[numPelicula]);
		pelicula.setDuracion(DURACION[numPelicula]);
		pelicula.setEdadMinima(EDADMIN[numPelicula]);
		
	}
	
	

}
