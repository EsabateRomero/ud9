package odt;

public class Asiento {
	private char letra = 0;
	private int fila = 0;
	private Espectador espectador = null;
	private boolean ocupado = false;
	
	public Asiento() {
		  
	}
	
	

	public Asiento(char letra, int fila) {
		this.letra = letra;
		this.fila = fila;
	}
	
	public Asiento(Espectador espectador) {
		if(!ocupado&&espectador.getDinero()>20) {
			this.espectador = espectador;
			this.ocupado = true;
		} 
	}



	public char getLetra() {
		return letra;
	}

	public void setLetra(char letra) {
		this.letra = letra;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public Espectador getEspectador() {
		return espectador;
	}

	public void setEspectador(Espectador espectador) {
		this.espectador = espectador;
	}

	public boolean isOcupado() {
		return ocupado;
	}

	public void setOcupado(boolean ocupado) {
		this.ocupado = ocupado;
	}

	@Override
	public String toString() {
		return "\n - Asiento [letra=" + letra + ", fila=" + fila + ", espectador=" + espectador + ", ocupado=" + ocupado
				+ "]";
	}
	
	
	
}
