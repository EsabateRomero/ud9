package odt;

import java.util.Arrays;

public class Cine {
	
	private Pelicula pelicula= new Pelicula();
	//prefereixo definir que totes les películes mirades costen 5 euros el seient. 
	private int precioEntrada = 20;
	private Asiento[][] asientos;
	private boolean incomplet = true;

	
	
	public boolean isIncomplet() {
		return incomplet;
	}

	public void ocuparSeient() {
		
	}

	public Cine() {
		
	}

	public Cine(Pelicula pelicula, int precioEntrada, int columnes, int files) {
		this.pelicula = pelicula;
		this.precioEntrada = precioEntrada;
		this.asientos = new Asiento[files][columnes];
		//etiquetar los asientos columna (char) y fila (int)
		darNombre(columnes, files, asientos);
	}
	
	public static void darNombre(int columnes, int files, Asiento[][] asientos) {
		int cont1 = 0;
		while(cont1<files) {
			int cont2 = 0;
			while(cont2<columnes) {
				asientos[cont1][cont2] = new Asiento((char)('a' + cont2), cont1);
				cont2++;
			}
			cont1++;
		}
	}
	


	public Pelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(Pelicula pelicula) {
		this.pelicula = pelicula;
	}

	public int getPrecioEntrada() {
		return precioEntrada;
	}

	public void setPrecioEntrada(int precioEntrada) {
		this.precioEntrada = precioEntrada;
	}



	@Override
	public String toString() {
		String tot = "";
		int cont = 0;
		while(cont<asientos.length) {
			int cont2 = 0;
			while(cont2<asientos[cont].length) {
				tot = tot + (asientos[cont][cont2]).toString();
				cont2++;
			}
			cont++;
		}
		
		return "Cine [pelicula=" + pelicula + ", precioEntrada=" + precioEntrada 
				+ tot ;
	}
	
	
	
}
