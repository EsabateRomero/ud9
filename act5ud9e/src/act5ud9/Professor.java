package act5ud9;

import java.util.Random;

public class Professor implements Asistir{
	
		final char HOMBRE = 'H';
		final char MUJER = 'M';
		
		String nombre = "";
		int edad = 0;
		char sexo = 'H';
		boolean estarDeBaja = estardeBaja();
		String materia = "";
	
		@Override
		public String toString() {
			return "\n	- Nombre: " + nombre +"\n	- Edad: " + edad + "\n	- sexo: " + sexo + "\n	- asistencia: " + estarDeBaja+"\n	- materia: "+materia;
		}
		
	public Professor() {
	
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		if(sexo.equalsIgnoreCase("hombre"))
			this.sexo = HOMBRE;
		else
			this.sexo = MUJER;
	}

	public Professor(String nombre, int edad, String sexo, String materia) {
		
		this.nombre = nombre;
		this.edad = edad;
		if(sexo.equalsIgnoreCase("hombre"))
			this.sexo = HOMBRE;
		else
			this.sexo = MUJER;
		this.materia = materia;
	}

	public boolean estardeBaja() {
		Random rand = new Random();
		int numero = rand.nextInt(5);
		if(numero==1)
			return false;
		else	
			return true;
	}
}
