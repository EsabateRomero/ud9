package act5ud9;

import java.util.ArrayList;

public class Aula {
	
	private int id;
	private String asignatura;
	private Professor Profesor;
	private Estudiante[] Estudiants;
	boolean clase;
	
	
	@Override
	public String toString() {
		
		
		int contador = 0; String estudiants = "";
		while(contador<Estudiants.length) {
			
			estudiants = estudiants + "\n\n" + Estudiants[contador].toString();
			contador++;
		}
		return "\n\n---------- AULA " + id +"-------------\n\nAsignatura: " + asignatura + "\n\n clase: " + clase + "\n\nProfesor: " + Profesor + "\n\nEstudiants:" + estudiants;
		
		
	}

	public Aula() {
		
	}
	
	public Aula(Professor profesor, Estudiante[] Estudiants) {
		
		if(profesor.materia.equalsIgnoreCase("matematicas"))
			this.id = 1;
		if(profesor.materia.equalsIgnoreCase("filosof�a"))
			this.id = 2;
		if(profesor.materia.equalsIgnoreCase("f�sica"))
			this.id = 3;
		this.asignatura = profesor.materia;
		this.Profesor = profesor;
		this.Estudiants = Estudiants;
		this.clase = mirarRequisits(Estudiants, profesor);
	}
	
	public static boolean mirarRequisits(Estudiante[] estudiants, Professor profesor) {
		
		int contador= 0, totalFaltes = 0; boolean esFaClasse = true;
		while(contador<estudiants.length) {
			if(estudiants[contador].novillos)
				totalFaltes++;
			contador++;
		}
		
		if(totalFaltes>=estudiants.length/2)
			esFaClasse = false;

		if(profesor.estarDeBaja)
			esFaClasse = false;

		
		return esFaClasse;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}

	public Professor getProfesor() {
		return Profesor;
	}

	public void setProfesor(Professor profesor) {
		Profesor = profesor;
	}

	public Estudiante[] getEstudiants() {
		return Estudiants;
	}

	public void setEstudiants(Estudiante[] estudiants) {
		Estudiants = estudiants;
	}
	
	

}
