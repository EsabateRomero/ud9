/**
 * 
 */
package act5ud9;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;


public class Estudiante implements Asistir{
	
	final char HOMBRE = 'H';
	final char MUJER = 'M';
	
	String nombre = "";
	int edad = 0;
	char sexo = 'H';
	boolean novillos = estardeBaja();
	static Hashtable<String, Integer> notasAsignatura = new Hashtable<String, Integer>();
		
	
	@Override
	public String toString() {
		return "\n\n - Nombre: " + nombre +"\n - Edad: " + edad + "\n - sexo: " + sexo + "\n - Asistencia: " + novillos;
	}

	
	public Estudiante() {
	
	}


	public Estudiante(String nombre, int edad, String sexo) {
		this.nombre = nombre;
		this.edad = edad;
		if(sexo.equalsIgnoreCase("hombre"))
			this.sexo = HOMBRE;
		else
			this.sexo = MUJER;
	}
	
	public boolean estardeBaja() {
		Random rand = new Random();
		int numero = rand.nextInt(2);
		if(numero==1)
			return true;
		else	
			return false;
	}
		
	public static Hashtable<String, Integer> getNotasAsignatura() {
		return notasAsignatura;
	}


	public static void setNotasAsignatura(Hashtable<String, Integer> notasAsignatura) {
		Estudiante.notasAsignatura = notasAsignatura;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}


	public char getSexo() {
		return sexo;
	}


	public void setSexo(String sexo) {
		if(sexo.equalsIgnoreCase("hombre"))
			this.sexo = HOMBRE;
		else
			this.sexo = MUJER;
	}
	
	public Integer notasAsignatura(String asignatura) {		
		Integer notaAsignatura = Obtenir(asignatura, notasAsignatura);
		return notaAsignatura;
	}
	
	public static Integer Obtenir(String asignatura, Hashtable<String, Integer> notasAsignatura) {
		return notasAsignatura.get(asignatura);
	}

	public boolean afegirAssignatura(String asignatura, ArrayList<Integer> notasEstudiante) {
		int notaAnterior = comprobarRegistreAsignatura(asignatura, notasAsignatura); 
		int mitjanaNotes = calcularMitjana(notasEstudiante);
		if(notaAnterior!=0)
			mitjanaNotes = (notaAnterior + mitjanaNotes)/2;
		if(asignatura.equalsIgnoreCase("matematicas")||asignatura.equalsIgnoreCase("filosof�a")||asignatura.equalsIgnoreCase("f�sica")) {
			if(notaAnterior!=0) {
				notasAsignatura.remove(asignatura);
				notasAsignatura.put(asignatura, mitjanaNotes);
				System.out.println("La nota ha estat actualitzada");
			} else {
				notasAsignatura.put(asignatura, mitjanaNotes);
				System.out.println("La nota ha estat afegida correctame");
			}
		} else
			return false;
			
			
			return true;
	}
	
	public static int comprobarRegistreAsignatura(String asignatura, Hashtable<String, Integer> notasAsignaturas) {
		Enumeration<String> asignaturas = notasAsignaturas.keys();
		Enumeration<Integer> notas = notasAsignaturas.elements();
		int notaMedia = 0;
		while(asignaturas.hasMoreElements()) {
			if(asignaturas.nextElement().equalsIgnoreCase(asignatura)) {
				System.out.println("Asignatura: " + asignaturas.nextElement() + " Nota: " + notas.nextElement());
				notaMedia = notas.nextElement();
			}
		}
		return notaMedia;
		
	}
	
	public static int calcularMitjana(ArrayList<Integer> notasEstudiante) {
		Integer[] notas = new Integer[notasEstudiante.size()];
		notas = notasEstudiante.toArray(notas);
		int contador = 0, totalNotas = 0, mitjana = 0;
		while(contador<notasEstudiante.size()) {
			totalNotas = totalNotas + notas[contador];
			contador++;
		}
		
		if(notasEstudiante.size()!=0)
		mitjana = totalNotas/notasEstudiante.size();
		
		return mitjana;
	}
	


}
