/**
 * 
 */
package act5ud9;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;


/**
 * @author Elisabet Sabat�
 *
 */
public class act5ud9App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Random random = new Random();
		final String[] asignaturas = {"matem�ticas","filosof�a","f�sica"};
		Estudiante[] estudiantes = new Estudiante[10];
		Professor[] profesores = new Professor[3];
		
		crearEstudiants(random, estudiantes, asignaturas);
		crearProfesors(random, profesores, asignaturas);
		Aula aula = new Aula(profesores[0], estudiantes);
		System.out.println(aula);
		Aula aula2 = new Aula(profesores[1], estudiantes);
		System.out.println(aula2);
		Aula aula3 = new Aula(profesores[2], estudiantes);
		System.out.println(aula3);
		
		}
	
	
	

	
	public static void mostrarProfessors(Professor[] profesors) {
		int contador = 0; 
		while(contador<profesors.length) {
			System.out.println(profesors[contador]);
			
			contador++;
		}
		
		
	}
		
	public static void mostrarEstudiants(Estudiante[] estudiantes) {
		int contador = 0; 
		while(contador<estudiantes.length) {
			System.out.println(estudiantes[contador]);
			
			contador++;
		}
		
		
	}
	
	
	public static void crearEstudiants(Random random, Estudiante[] estudiantes, String[] asignaturas) {
		int contador = 0; 
		while(contador<estudiantes.length) {
				
			estudiantes[contador] = new Estudiante();
				
			obtenerNombreSexoEdad(random, estudiantes[contador], asignaturas, contador);
				
			contador++;
		}
			
			
	}
		
	public static void crearProfesors(Random random, Professor[] profesor, String[] asignaturas) {
		int contador = 0; 
		while(contador<profesor.length) {
				
			profesor[contador] = new Professor();
				
			obtenerNombreSexoEdad(random, profesor[contador], asignaturas, contador);
				
			contador++;
		}	
			
	}
	
	public static void afegirNotes(Estudiante estudiante, Random random) {
		ArrayList<Integer> notasM = new ArrayList<Integer>();
		notasM.add(random.nextInt(10));
		notasM.add(random.nextInt(10));
		notasM.add(random.nextInt(10));
		
		estudiante.afegirAssignatura("matematicas", notasM);
		
		ArrayList<Integer> notasF = new ArrayList<Integer>();
		notasF.add(random.nextInt(10));
		notasF.add(random.nextInt(10));
		notasF.add(random.nextInt(10));
		
		estudiante.afegirAssignatura("filosof�a", notasF);
		
		
		ArrayList<Integer> notasF2 = new ArrayList<Integer>();
		notasF2.add(random.nextInt(10));
		notasF2.add(random.nextInt(10));
		notasF2.add(random.nextInt(10));
		
		estudiante.afegirAssignatura("f�sica", notasF2);
		
		
	}
	
	public static void obtenerNombreSexoEdad(Random random, Object persona, String[] asignaturas, int contador) {
		final String[] nombresMujer = {"M�nica","Mireia","Joana","Ramona","Sonia"}, nombresHombre = {"Miguel","Joan","Marc","Albert","Antoni"};
		if(persona instanceof Estudiante) {
			Estudiante estudiante=(Estudiante)persona;
			if(random.nextInt(2)==1) {
				estudiante.setNombre(nombresMujer[random.nextInt(5)]);
				estudiante.setSexo("mujer");
			} else {
				estudiante.setNombre(nombresHombre[random.nextInt(5)]);
				estudiante.setSexo("hombre");
			}
				
			estudiante.setEdad(random.nextInt(4)+16);
			
			//afegirNotes(estudiante, random);
					
		} 

		if(persona instanceof Professor) {
			Professor profesor=(Professor)persona;
			if(random.nextInt(2)==1) {
				profesor.setNombre(nombresMujer[random.nextInt(5)]);
				profesor.setSexo("mujer");
			} else {
				profesor.setNombre(nombresHombre[random.nextInt(5)]);
				profesor.setSexo("hombre");
			}
				
			profesor.setEdad(random.nextInt(15)+22);
				
			profesor.setMateria(asignaturas[contador]);
				
				
		}
				
	}

	
	
	
}
	
	
		
		
		

	


