package dto;

import java.util.Random;

public class Libro {
	Random random = new Random();
	
	final private String[] TITULOS = {"libro1","libro2","libro3","libro4","libro5","libro6"};
	final private String[] AUTORES = {"autor1 apellido","autor2 apellido","autor3 apellido","autor4 apellido","autor5 apellido","autor6 apellido"};
	private int numeroPaginas = random.nextInt(99)+1;
	private String titulo = TITULOS[random.nextInt(TITULOS.length)], autor = AUTORES[random.nextInt(AUTORES.length)], ISBN = randomISBN(random);
	
	public Libro() {
		
	}

	public Libro(String iSBN, int numeroPaginas, String titulo, String autor) {
		
		this.ISBN = iSBN;
		this.numeroPaginas = numeroPaginas;
		this.titulo = titulo;
		this.autor = autor;
	}
	
	public static String randomISBN(Random random) {
		int contador = 0; String ISBN = "";
		while(contador<13) {
			if(contador==3|contador==4|contador==6|contador==12)
				ISBN = ISBN + "-";
			ISBN = ISBN + random.nextInt(9);
			contador++;
		}
		return ISBN;
	}
		
	public int getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(int numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	@Override
	public String toString() {
		return "El libro \""+titulo+"\" con isbn " + ISBN +" creado por el autor " + autor + " tiene "+numeroPaginas+" p�ginas";
	}
		

}
