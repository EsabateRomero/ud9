
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import javax.sound.midi.Soundbank;

import dto.Libro;




/**
 * @author Elisabet Sabat�
 *
 */
public class act2ud9App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		
		Libro llibre1 = new Libro();
		Libro llibre2 = new Libro();	

		System.out.println("\n" + llibre1);
		System.out.println("\n" + llibre2);
		
		
		System.out.println("\n\n--------- QUIN �S M�S GRAN? ----------------");
		if(llibre1.getNumeroPaginas()>llibre2.getNumeroPaginas())
			System.out.println("El llibre \""+llibre1.getTitulo()+"\" t� m�s p�gines");
		else if(llibre1.getNumeroPaginas()<llibre2.getNumeroPaginas())
			System.out.println("El llibre \""+llibre2.getTitulo()+"\" t� m�s p�gines");
		else
			System.out.println("Tenen les mateixes p�gines.");
	}

}
